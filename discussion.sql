INSERT INTO artists (name) VALUES 
("Taylor Swift"), 
("Lady Gaga"),
("Justin Bieber"),
("Ariana Grande"),
("Bruno Mars");
/* 
Taylor Swift - id 3
Lady Gaga - id 4
Justin Bieber - id 5
Ariana Grande - id 6
Bruno Mars - id 7
*/

/* Taylor Swift*/
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-01-01", 3);

INSERT INTO songs (song_name, length, genre, album_id) VALUES 
	("Fearless", 246, "Pop rock", 3),
	("Love Story", 213, "Country pop", 3);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-01-01", 3); /* album id - 4*/
INSERT INTO songs (song_name, length, genre, album_id) VALUES 
	("State of Grace", 250, "Rock, alternative rock, arena rock", 4),
	("Red", 204, "Country", 4);

/* Lady Gaga */
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star is Born", "2018-01-01", 4); --5
INSERT INTO songs (song_name, length, genre, album_id) VALUES 
	("Black Eyes", 151, "Rock and roll", 5),
	("Shallow", 201, "Country, rock, folk rock", 5);

-- 6
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-01-01", 4); 
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 6);

-- Justin Bieber (4)
-- album_id 7
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015-01-01", 5); 
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 200, "Dancehall-poptropical housemoombathon", 7);

-----------------------------------

/* ADVANCED SELECTS */

-- Excluding records (!=)
SELECT * FROM songs WHERE id != 1;

-- Greater than, less than, or equal
SELECT * FROM songs WHERE id > 1;
SELECT * FROM songs WHERE id < 9;
SELECT * FROM songs WHERE id = 1;

-- OR
SELECT * FROM songs WHERE id = 1 OR id = 5;

-- IN
SELECT * FROM songs WHERE id IN (1, 3, 5);
SELECT * FROM songs WHERE genre IN ("OPM", "Electropop");

-- Combining Conditions
SELECT * FROM songs WHERE genre = "OPM" AND length > 230; 

-- Find partial matches
-- ENDS WITH
SELECT * FROM songs WHERE song_name LIKE "%a";
-- STARTS WITH
SELECT * FROM songs WHERE song_name LIKE "b%";
-- SELECT keyword in between
SELECT * FROM songs WHERE song_name LIKE "%a%";

SELECT * FROM albums WHERE date_released LIKE "201_-__-__";


SELECT * FROM songs WHERE song_name LIKE "b%" AND song_name LIKE "_____ Eyes" ;

-- Finding song_name with second character e
SELECT * FROM songs WHERE song_name LIKE "_e%";

-- Sorting records (ORDER BY)
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- DISTINCT
SELECT DISTINCT genre FROM songs; 

-- Table Joins
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id;

/* SELECT * FROM ReferenceTable JOIN tableName ON ReferenceTable.PrimaryKey = TableName.foreignKey */

SELECT album_title, artists.name FROM albums
	JOIN artists ON albums.artist_id = artists.id;

-- Combine more thant two tables
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

/* SELECT * FROM table1 
	JOIN table2 ON table1.primarykey = table2.foreignkey
	JOIN table3 ON table2.primarykey = table3.foreignkey */

SELECT songs.song_name FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

-- Select columns to be included per table
SELECT artists.name, albums.album_title FROM artists
	JOIN albums ON artists.id = albums.artist_id;

/* UNION */
SELECT artist.name FROM artists UNION SELECT albums.album_title FROM albums

/* INNER JOIN */
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id

/* LEFT JOIN */
SELECT * FROM artists
	LEFT JOIN albums ON artist.id = albums.artist_id;

/* RIGHT JOIN */
SELECT * FROM albums
	RIGHT JOIN artists ON albums.artist_id = artists.id;
